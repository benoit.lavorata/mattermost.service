#!/bin/bash
echo ""
echo "Mattermost service"
echo "Maintainer: Benoit Lavorata"
echo "License: MIT"
echo "==="
echo ""

FILE=".env"
if [ -f $FILE ]; then
    echo ""
    echo "File $FILE exists, will now start: ..."
    export $(cat .env | xargs)

    echo ""
    echo "Create networks ..."
    docker network create $MATTERMOST_CONTAINER_NETWORK
    docker network create $DB_CONTAINER_NETWORK

    echo ""
    echo "Create volumes ..."
    docker volume create --name ${MATTERMOST_CONTAINER_VOLUME_ROOT}_config
    docker volume create --name ${MATTERMOST_CONTAINER_VOLUME_ROOT}_data
    docker volume create --name ${MATTERMOST_CONTAINER_VOLUME_ROOT}_logs
    docker volume create --name ${MATTERMOST_CONTAINER_VOLUME_ROOT}_plugins
    docker volume create --name ${MATTERMOST_CONTAINER_VOLUME_ROOT}_client_plugins
    docker volume create --name $DB_CONTAINER_VOLUME_DATA
    
    echo "Make sure to set up admin password and config !"

    echo ""
    echo "Boot ..."
    docker-compose up -d --remove-orphans $1
else
    echo "File $FILE does not exist: deploying for first time"
    cp .env.template .env
    
    echo -e "Make sure to modify .env according to your needs, then use ./up.sh again."
fi

