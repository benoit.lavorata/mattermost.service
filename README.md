# mattermost.service
Based on https://mattermost.com/

# Iframe embedding with nginx
Mattermost does not allow embedding by default, so we strip out the header that restricts it:   
proxy_hide_header X-Frame-Options;
proxy_hide_header Content-Security-Policy;

add_header 'X-Frame-Options' "ALLOW-FROM https://demo.domain.local" always;  
add_header 'Content-Security-Policy' "frame-ancestors https://demo.domain.local" always;    
add_header 'Access-Control-Allow-Origin' "https://demo.domain.local" always; 
add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS, DELETE, PUT';  
add_header 'Access-Control-Allow-Credentials' 'true' always;   
add_header 'Access-Control-Allow-Headers' 'User-Agent,Keep-Alive,Content-Type';   
